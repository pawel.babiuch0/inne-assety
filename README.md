# inne-assety

Pojedyncze i indywidualne assety, które wykorzystuję modułowo przy innych projektach.

P.S. Nie wszystkie zostały ukończone [*<|:-)](https://youtu.be/WY089rnDo2o)

## Spis projektów

 - [Menu kołowe](#menu-kołowe)
 - [Auto text-sizer](#auto-text-sizer)
 - [LanguageSystem](#LanguageSystem)

#

### Menu kołowe

Asset wykorzystywany do projektów VR bazujących na systemie SteamVR. Umożliwa dodanie modułowego menu kołowego przy kontrolerze: [LINK DO FILMU](https://youtu.be/tbN6GZneKyE).

#

### Auto text sizer

Asset do automatyczengo skalowania wielkości fonta. Często się zapomina o wyrównaniu czcionki do najmniejszej możliwej, w celu zapewnienia ładnego dla oka UI. Ten asset nieco ułatwia życie, [LINK](https://youtu.be/E3MOfwTQQ8U).

#

### LanguageSystem

Paczka umożliwiająca, w łatwy sposób zmianę Języka aplikacji, link do wycinku paczki: [LINK](https://drive.google.com/file/d/1WjlZIjsALx2R8e97L56wslq1xqWvXHqj/view?usp=sharing).

Paczka nie zawiera całej zawartości i w razie czego należy zmienić w pliku Language Manager Settings typ pobierania pliku, z URL na Resources:
<img src="https://drive.google.com/file/d/1Hz9WVigT0iqeSd1ZU8bqh3SK2GJk6Es8/view?usp=sharing" alt="Language Manager Settings" border="10" />

